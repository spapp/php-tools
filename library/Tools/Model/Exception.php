<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_tools
 * @subpackage Model
 * @since     2014.08.21.
 */

namespace Tools\Model;

/**
 * Class Exception
 */
class Exception extends \Exception{

} 