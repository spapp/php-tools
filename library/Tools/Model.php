<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_tools
 * @subpackage Tools
 * @since      2014.08.21.
 */

namespace Tools;

use Tools\Model\Exception as ModelException;

require_once('Model/Exception.php');

/**
 * Class Model
 *
 * @method mixed getId() returns current id or default value
 * @method mixed setId(mixed $id) sets current id value and return $this
 */
abstract class Model {
    const PROPERTY_TYPE     = 'type';
    const PROPERTY_PATTERN  = 'pattern';
    const PROPERTY_REQUIRED = 'required';
    const PROPERTY_ERROR    = 'error';
    const PROPERTY_DEFAULT  = 'default';

    const DEFAULT_PROPERTY_ERROR = 'The "%s" is invalid.';
    /**
     * @var array error messages
     * @static
     */
    protected static $errorMessages = array(
            PREG_NO_ERROR              => 'There were no errors.',
            PREG_INTERNAL_ERROR        => 'There was an internal PCRE error.',
            PREG_BACKTRACK_LIMIT_ERROR => 'Backtrack limit was exhausted.',
            PREG_RECURSION_LIMIT_ERROR => 'Recursion limit was exhausted.',
            PREG_BAD_UTF8_ERROR        => 'Malformed UTF-8 data.',
            PREG_BAD_UTF8_OFFSET_ERROR => "The offset didn't correspond to the begin of a valid UTF-8 code point.",
            'missing_arguments'        => 'Missing arguments!',
            'not_supported_method'     => 'The "%s" method is not supported!',
            'not_supported_property'   => 'The "%s" property is not supported!'
    );

    /**
     * @var array allowed data properties
     */
    protected $allowed = array(
            'id' => array(
                    self::PROPERTY_TYPE => 'integer'
            )
    );
    /**
     * @var array data container
     */
    protected $data = array();
    /**
     * @var array data errors container
     */
    protected $dataErrors = array();

    /**
     * Magic method
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return Model|null|mixed
     * @throws ModelException
     */
    public final function __call($name, $arguments) {
        $action      = strtolower(substr($name, 0, 3));
        $popertyName = lcfirst(substr($name, 3));
        $return      = $this;

        if ('get' === $action and $this->isAllowedProperty($popertyName)) {
            if (array_key_exists($popertyName, $this->data)) {
                $return = $this->data[$popertyName];
            } elseif (array_key_exists(self::PROPERTY_DEFAULT, $this->allowed[$popertyName])) {
                $return = $this->allowed[$popertyName][self::PROPERTY_DEFAULT];
            } else {
                $return = null;
            }
        } elseif ('set' === $action and $this->isAllowedProperty($popertyName)) {
            if (1 === count($arguments)) {
                $this->data[$popertyName] = $arguments[0];
            } else {
                throw new ModelException(self::$errorMessages['missing_arguments']);
            }
        } else {
            throw new ModelException(sprintf(self::$errorMessages['not_supported_method'], $name));
        }

        return $return;
    }

    /**
     * Returns TRUE if the model data is valid.
     *
     * @return bool
     */
    public final function isValid() {
        $this->validate();

        return (count($this->dataErrors) < 1);
    }

    /**
     * Validates the  model data.
     *
     * @return $this
     */
    public final function validate() {
        $this->dataErrors = array();

        foreach ($this->data as $name => $value) {
            if (false === $this->isValidProperty($name, $value)) {
                if (false === array_key_exists($name, $this->dataErrors)) {
                    $this->dataErrors[$name] = array();
                }

                array_push($this->dataErrors[$name], $this->getPropertyError($name));
            }
        }

        return $this;
    }

    /**
     * Returns error messages if the data is not valid.
     *
     * @return array
     */
    public final function getErrors() {
        return $this->dataErrors;
    }

    /**
     * Returns TRUE if the property is required.
     *
     * @param string $name
     *
     * @return bool
     */
    public final function isRequired($name) {
        if (true === $this->isAllowedProperty($name) and
            true === array_key_exists(self::PROPERTY_REQUIRED, $this->allowed[$name]) and
            true === $this->allowed[$name][self::PROPERTY_REQUIRED]
        ) {
            return $this->allowed[$name][self::PROPERTY_REQUIRED];
        }

        return false;
    }

    /**
     * Returns TRUE if the property is allowed otherwise returns FALSE
     *
     * @param string $name property name
     *
     * @return bool
     */
    public final function isAllowedProperty($name) {
        return array_key_exists($name, $this->allowed);
    }

    /**
     * Returns TRUE if the property is exists.
     *
     * @param string $name
     *
     * @return bool
     */
    public final function hasProperty($name) {
        return array_key_exists($name, $this->data[$name]);
    }

    /**
     * Constructor
     */
    public function __construct(array $data = array()) {
        foreach ($data as $name => $value) {
            $method = 'set' . ucfirst($name);
            call_user_func_array(array($this, $method), array($value));
        }
    }

    /**
     * Destructor
     */
    public function __destruct() {

    }

    /**
     * Magic method
     *
     * @return string
     */
    public function __toString() {
        return print_r($this->data, true);
    }

    /**
     * Returns a property error message.
     *
     * If it is not exists then return default error message.
     *
     * @see self::DEFAULT_PROPERTY_ERROR
     *
     * @param string $name
     *
     * @return string
     * @throws Model\Exception
     */
    protected final function getPropertyError($name) {
        if (true === $this->isAllowedProperty($name)) {
            $option = (array)$this->allowed[$name];

            if (array_key_exists(self::PROPERTY_ERROR, $option)) {
                $errorMessage = $option[self::PROPERTY_ERROR];
            } else {
                $errorMessage = sprintf(self::DEFAULT_PROPERTY_ERROR, $name);
            }
        } else {
            throw new ModelException(sprintf(self::$errorMessages['not_supported_property'], $name));
        }

        return $errorMessage;
    }

    /**
     * Returns TRUE if the property value is valid.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return bool
     * @throws Model\Exception
     */
    protected final function isValidProperty($name, $value) {
        $isValid = true;

        if (true === $this->isAllowedProperty($name)) {
            $option = (array)$this->allowed[$name];

            if (true === $this->isRequired($name) and false === $this->hasProperty($name)) {
                $isValid = false;
            } elseif (array_key_exists(self::PROPERTY_TYPE, $option)) {
                $isValid = ($option[self::PROPERTY_TYPE] === gettype($value));
            } elseif (array_key_exists(self::PROPERTY_PATTERN, $option)) {
                $isValid = preg_match($option[self::PROPERTY_PATTERN], $value);

                if (false === $isValid) {
                    throw new ModelException(self::$errorMessages[preg_last_error()]);
                }

                $isValid = (bool)$isValid;
            }
        } else {
            $isValid = false;
        }

        return $isValid;
    }
} 