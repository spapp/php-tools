<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2014
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package    php_tools
 * @subpackage Tools
 * @since      2014.08.22.
 */

namespace Tools;

/**
 * Class String
 */
class String {
    const DEFAULT_DELIMITER = '-';
    /**
     * @var string
     */
    protected $text = '';

    /**
     * Constructor
     */
    public function __construct($text = '') {
        $this->setText($text);
    }

    /**
     * Replace all the special characters.
     *
     * @param string $delimiter replacement character
     * @param array  $replace   unnecessary characters
     *
     * @return string the new ascii string
     */
    function toAscii($delimiter = self::DEFAULT_DELIMITER,array $replace = array()) {
        if (!empty($replace)) {
            $this->text = str_replace($replace, ' ', $this->text);
        }

        $this->text = iconv('UTF-8', 'ASCII//TRANSLIT', $this->text);
        $this->text = preg_replace("~[^a-zA-Z0-9/_|+ -]~", '', $this->text);
        $this->text = strtolower(trim($this->text, '-'));
        $this->text = preg_replace("~[/_|+ -]+~", $delimiter, $this->text);

        return $this;
    }

    /**
     * Sets up text value.
     *
     * @param $text
     *
     * @return $this
     */
    public function setText($text) {
        $this->text = (string)$text;

        return $this;
    }

    /**
     * Magic method
     *
     * @return string
     */
    public function __toString() {
        return $this->text;
    }

    /**
     * Destructor
     */
    public function __destruct() {

    }
}
