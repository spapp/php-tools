<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_tools
 * @since     2014.08.21.
 */

require_once('../library/Tools/Model.php');

/**
 * Class Message
 *
 * @method mixed getId() returns current id
 * @method mixed setId(mixed $id) sets current id value and return $this
 * @method mixed getMessage() returns current message
 * @method mixed setMessage(string $message) sets current message value and return $this
 */
class Message extends \Tools\Model {
    protected $allowed = array(
            'id'      => array(
                    self::PROPERTY_TYPE => 'integer'
            ),
            'message' => array(
                    self::PROPERTY_TYPE => 'string'
            )
    );
}


$message = new Message(
        array(
                'id'      => 1,
                'message' => 'test message'
        )
);

echo PHP_EOL . $message;
echo PHP_EOL . 'isValid:' . ($message->isValid() ? 'TRUE' : 'FALSE') . PHP_EOL;
print_r($message->getErrors());

$message->setMessage('new message');

echo PHP_EOL . $message;

$message->setMessage(2);
echo PHP_EOL . $message;
echo PHP_EOL . 'isValid:' . ($message->isValid() ? 'TRUE' : 'FALSE') . PHP_EOL . PHP_EOL;

print_r($message->getErrors());

$message->setMessage2('new message');

