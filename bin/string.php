<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_tools
 * @since     2014.08.22.
 */

require_once(dirname(dirname(__FILE__)) . '/library/Tools/String.php');

/**
 * Set locale information
 */
setlocale(LC_ALL, 'hu_HU.UTF8');

/**
 * test data in different languages
 */
$test = array(
        'Dette er en test tekst. Vinter, sommer, "forår og efterår".',
        'Ky është një tekst test. , Dimër verë, pranverë dhe në vjeshtë.',
        'This is a test text. Winter, summer, spring and autumn.',
        'Bu test mətn. Qış, yay, yaz və payız.',
        'Гэта тэст тэксту. Узімку, летам, вясной і восенню.',
        'Tämä on testi tekstiä. Talvi, kesä, kevät ja syksy.',
        "Il s'agit d'un texte de test. Hiver, été, printemps et automne.",
        'Este é un exame de texto. Inverno, primavera, verán e outono.',
        'Αυτό είναι ένα δοκιμαστικό κείμενο. Χειμώνας, καλοκαίρι, άνοιξη και φθινόπωρο.',
        'ეს არის გამოცდა ტექსტი. ზამთრის, ზაფხულის, გაზაფხულზე და შემოდგომაზე.',
        'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöùúûüýÿ',
        '',
        'Magyar:       eeőüóűúáéß',
        'Álomalak a nyaralásra? Ugye,     jól hangzik?',
        'Kalandra fel, indul az első Echo Fesztivál!',
        'Mediterrán ízek szlovén módra...'
);

$string = new \Tools\String();

echo str_pad('AFTER', 70) . "\tBEFORE" . PHP_EOL;

foreach (array(\Tools\String::DEFAULT_DELIMITER, '_') as $delimiter) {
    echo str_pad('', 150, '-') . PHP_EOL;

    foreach ($test as $value) {
        echo str_pad('"' . $string->setText($value)->toAscii($delimiter) . '"', 70) . "\t\"" . $value . '"' .
             PHP_EOL;
    }
}
